﻿using Paulo.MarvelSearch.Contracts;
using Paulo.MarvelSearch.Data;
using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Paulo.MarvelSearch.Services
{
    public class UserService : IUserService
    {
        const string SALT = "e066a42c0a3185bc16d7688b4fbc2382";

        protected readonly MarvelSearchContext dataContext;
        public UserService(MarvelSearchContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public User Login(string username, string password)
        {
            return dataContext.Set<User>()
                .FirstOrDefault(p => p.Username == username && p.Password == HashPassword(username, password));
        }

        public void CreateUser(string username, string password)
        {
            if (!dataContext.Set<User>().Any(p => p.Username == username))
            {
                User user = new User();
                user.Username = username;
                user.Password = HashPassword(username, password);
                dataContext.Add(user);
                dataContext.SaveChanges();
            }
            else
            {
                throw new Exception("Username already taken");
            }
            
        }

        private string HashPassword(string username, string password)
        {
            var md5 = MD5.Create();
            byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes($"{username}{password}{SALT}"));
            return Convert.ToBase64String(hash);
        }
    }
}
