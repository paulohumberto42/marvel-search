﻿using Paulo.MarvelSearch.Contracts;
using Paulo.MarvelSearch.Data;
using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paulo.MarvelSearch.Services
{
    public class SearchLogService : ISearchLogService
    {
        protected readonly MarvelSearchContext dataContext;
        public SearchLogService(MarvelSearchContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Log(string characterName)
        {
            SearchLog searchLog = new SearchLog();
            searchLog.CharacterName = characterName;
            searchLog.Date = DateTime.Now;

            this.dataContext.Add(searchLog);
            this.dataContext.SaveChanges();
        }

        public List<SearchLog> GetLastLogs(int count)
        {
            return this.dataContext.Set<SearchLog>()
                .OrderByDescending(p => p.Date)
                .Take(count)
                .ToList();
        }
    }
}
