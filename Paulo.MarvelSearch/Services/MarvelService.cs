﻿using Paulo.MarvelSearch.Api;
using Paulo.MarvelSearch.Contracts;
using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paulo.MarvelSearch.Services
{
    public class MarvelService : IMarvelService
    {
        protected readonly MarvelApiClient marvelApiClient;
        protected readonly ISearchLogService searchLogService;

        public MarvelService(MarvelApiClient marvelApiClient, ISearchLogService searchLogService)
        {
            this.marvelApiClient = marvelApiClient;
            this.searchLogService = searchLogService;
        }

        public Character GetCharacter(string name)
        {
            this.searchLogService.Log(name);

            var apiResult = this.marvelApiClient.Characters(name);

            if (apiResult?.Data?.Results?.Any() ?? false)
            {
                var apiCharacter = apiResult.Data.Results[0];
                return CreateFromApi(apiCharacter);
            }

            return null;
        }

        private Character CreateFromApi(Api.Model.Character apiCharacter)
        {
            if (apiCharacter != null)
            {
                Character character = new Character();
                character.ID = apiCharacter.Id;
                character.Name = apiCharacter.Name;
                character.Description = apiCharacter.Description;
                character.ImageUrl = $"{apiCharacter.Thumbnail.Path}.{apiCharacter.Thumbnail.Extension}";
                character.Stories = apiCharacter.Stories.Items.Select(p => p.Name).ToArray();

                return character;
            }
            else
            {
                return null;
            }
        }
    }
}
