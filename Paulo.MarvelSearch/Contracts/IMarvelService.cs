﻿using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paulo.MarvelSearch.Contracts
{
    public interface IMarvelService
    {
        Character GetCharacter(string name);
    }
}
