﻿using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paulo.MarvelSearch.Contracts
{
    public interface IUserService
    {
        User Login(string username, string password);
        void CreateUser(string username, string password);
    }
}
