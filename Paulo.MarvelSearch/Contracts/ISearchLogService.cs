﻿using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paulo.MarvelSearch.Contracts
{
    public interface ISearchLogService
    {
        void Log(string characterName);
        List<SearchLog> GetLastLogs(int count);
    }
}
