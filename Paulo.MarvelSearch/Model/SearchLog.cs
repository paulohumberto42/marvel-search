﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paulo.MarvelSearch.Model
{
    public class SearchLog
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string CharacterName { get; set; }
    }
}
