﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paulo.MarvelSearch.Model
{
    public class Character
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string[] Stories { get; set; }
    }
}
