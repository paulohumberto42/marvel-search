﻿using Newtonsoft.Json;
using Paulo.MarvelSearch.Api.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Paulo.MarvelSearch.Api
{
    public class MarvelApiClient
    {
        private readonly MD5 md5;
        private readonly IRestClient innerClient;
        private string publicKey;
        private string privateKey;

        public MarvelApiClient(string publicKey, string privateKey)
        {
            this.md5 = MD5.Create();

            this.innerClient = new RestClient("https://gateway.marvel.com");
            this.publicKey = publicKey;
            this.privateKey = privateKey;
        }

        public CharacterDataWrapper Characters(string name)
        {
            IRestRequest request = new RestRequest("/v1/public/characters", Method.GET);
            request.AddParameter("name", name);
            return ExecuteRequest<CharacterDataWrapper>(request);
        }

        private T ExecuteRequest<T>(IRestRequest request)
        {
            // Autentica a requisição
            this.AuthenticateRequest(request);

            // Executa a Requisição
            IRestResponse response = this.innerClient.Execute(request);

            // Trata a exceção, caso seja de API, recupera o código e a mensagem
            if (response.IsSuccessful)
            {
                var result = JsonConvert.DeserializeObject<T>(response.Content);
                return result;
            }
            else if (response.ContentType == "application/json; charset=utf-8")
            {
                var ex = JsonConvert.DeserializeObject<ApiExceptionDetails>(response.Content);
                throw ex.ToException(request, response);
            }
            else
            {
                // TODO: Tratar erros que não são tratados pela API
                throw new Exception(response.Content);
            }
        }

        private void AuthenticateRequest(IRestRequest request)
        {
            string ts = $"{(DateTime.UtcNow - DateTime.UnixEpoch).TotalSeconds:f0}";
            string authParameters = $"{ts}{this.privateKey}{this.publicKey}";
            byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(authParameters));
            string formattedHash = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();

            request.AddParameter("apikey", this.publicKey);
            request.AddParameter("ts", ts);
            request.AddParameter("hash", formattedHash);
        }
    }
}
