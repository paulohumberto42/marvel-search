﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paulo.MarvelSearch.Data
{
    public class MarvelSearchContext : DbContext
    {
        public static readonly SqliteConnection MEMORY_CONNECTION = new SqliteConnection("DataSource=:memory:");

        public MarvelSearchContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(p => p.Username)
                .IsUnique(true);

            modelBuilder.Entity<SearchLog>();

            base.OnModelCreating(modelBuilder);
        }
    }
}
