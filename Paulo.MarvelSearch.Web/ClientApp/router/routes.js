import CharacterSearch from 'components/character-search'

export const routes = [
  { name: 'home', path: '/', component: CharacterSearch, display: 'Home', icon: 'home' },
]
