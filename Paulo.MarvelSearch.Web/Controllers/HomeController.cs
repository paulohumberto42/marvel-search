using Microsoft.AspNetCore.Mvc;

namespace Paulo.MarvelSearch.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Test([FromServices] Contracts.IMarvelService x)
        {
            return Content("oi");
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
