using Microsoft.AspNetCore.Mvc;
using Paulo.MarvelSearch.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paulo.MarvelSearch.Web.Controllers
{
    [Route("api/[controller]")]
    public class MarvelController : Controller
    {
        IMarvelService marvelService;

        public MarvelController(IMarvelService marvelService)
        {
            this.marvelService = marvelService;
        }

        [HttpGet("[action]")]
        public IActionResult Character(string name)
        {
            var character = marvelService.GetCharacter(name);

            if (character != null)
            {
                return Ok(character);
            }

            return NotFound();
        }
    }
}
