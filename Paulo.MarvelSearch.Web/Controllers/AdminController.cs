using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paulo.MarvelSearch.Contracts;
using Paulo.MarvelSearch.Web.Attributes;
using Paulo.MarvelSearch.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paulo.MarvelSearch.Web.Controllers
{
    public class AdminController : Controller
    {
        public const string USER_SESSION_KEY = "USER";
        readonly ISearchLogService searchLogService;
        readonly IUserService userService;

        public AdminController(ISearchLogService searchLogService, IUserService userService)
        {
            this.searchLogService = searchLogService;
            this.userService = userService;
        }

        [HttpGet]
        [RequireAuthentication]
        public IActionResult Index()
        {
            var viewModel = new SearchLogViewModel();
            viewModel.Logs = searchLogService.GetLastLogs(100);
            return View(viewModel);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Login(LoginViewModel data)
        {
            var user = userService.Login(data.Username, data.Password);

            if (user != null)
            {
                HttpContext.Session.SetString(USER_SESSION_KEY, user.Username);
                return RedirectToAction("Index");
            }
            else
            {
                data.Message = "Invalid username/password";
                return View(data);
            }
        }
    }
}
