using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Paulo.MarvelSearch.Api;
using Paulo.MarvelSearch.Contracts;
using Paulo.MarvelSearch.Data;
using Paulo.MarvelSearch.Services;
using System;

namespace Paulo.MarvelSearch.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                options.Cookie.HttpOnly = true;
            });

            // Add framework services.
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddTransient<MarvelApiClient>(provider =>
            {
                var configuration = provider.GetService<IConfiguration>();
                var marvelConfiguration = configuration.GetSection("MarvelApi");
                return new MarvelApiClient(marvelConfiguration["PublicKey"], marvelConfiguration["PrivateKey"]);
            });

            services.AddDbContext<MarvelSearchContext>(options => options.UseSqlite(Configuration.GetConnectionString("MarvelSearchContext")));

            services.AddTransient<IMarvelService, MarvelService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ISearchLogService, SearchLogService>();

            var serviceProvider = services.BuildServiceProvider();

            using (var scope = serviceProvider.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<MarvelSearchContext>();
                context.Database.EnsureCreated();

                var userService = scope.ServiceProvider.GetRequiredService<IUserService>();

                if (userService.Login("Admin", "123456") == null)
                {
                    userService.CreateUser("Admin", "123456");
                }
            }

            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // Webpack initialization with hot-reload.
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true,
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
