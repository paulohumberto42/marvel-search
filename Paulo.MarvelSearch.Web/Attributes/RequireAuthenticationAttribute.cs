using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Paulo.MarvelSearch.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paulo.MarvelSearch.Web.Attributes
{
    public class RequireAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string user = context.HttpContext.Session.GetString(AdminController.USER_SESSION_KEY);

            if (user != null)
            {
                base.OnActionExecuting(context);
            }
            else
            {
                context.Result = new RedirectToActionResult("Login", "Admin", null);
            }
        }
    }
}
