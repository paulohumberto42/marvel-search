using Paulo.MarvelSearch.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Paulo.MarvelSearch.Web.Models
{
    public class SearchLogViewModel
    {
        public List<SearchLog> Logs { get; set; }
    }
}
